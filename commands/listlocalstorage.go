package commands

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"git.roshless.me/roshless/mumbledj/interfaces"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"layeh.com/gumble/gumble"
)

// ListLocalStorageCommand is a command that lists the tracks that were
// added to Local Storage service.
type ListLocalStorageCommand struct{}

// Aliases returns the current aliases for the command.
func (c *ListLocalStorageCommand) Aliases() []string {
	return viper.GetStringSlice("commands.listlocalstorage.aliases")
}

// Description returns the description for the command.
func (c *ListLocalStorageCommand) Description() string {
	return viper.GetString("commands.listlocalstorage.description")
}

// IsAdminCommand returns true if the command is only for admin use, and
// returns false otherwise.
func (c *ListLocalStorageCommand) IsAdminCommand() bool {
	return viper.GetBool("commands.listlocalstorage.is_admin")
}

// Execute executes the command with the given user and arguments.
// Return value descriptions:
//
//	string: A message to be returned to the user upon successful execution.
//	bool:   Whether the message should be private or not. true = private,
//	        false = public (sent to whole channel).
//	error:  An error message to be returned upon unsuccessful execution.
//	        If no error has occurred, pass nil instead.
//
// Example return statement:
//
//	return "This is a private message!", true, nil
func (c *ListLocalStorageCommand) Execute(user *gumble.User, args ...string) (string, bool, error) {

	var (
		message   string
		tag       string
		allTracks []interfaces.Track
		tracks    []interfaces.Track
		service   interfaces.Service
		err       error
	)

	files, err := os.ReadDir(viper.GetString("localstorage.directory"))
	if err != nil {
		return "", true, errors.New(viper.GetString("localstorage.common_messages.directory_error"))
	}

	if len(files) == 0 {
		return "", true, errors.New(viper.GetString("localstorage.common_messages.no_songs_error"))
	}

	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".track") {
			tag = f.Name() + ".ls"
			if service, err = DJ.GetService(tag); err == nil {
				tracks, err = service.GetTracks(tag, user)
				if err != nil {
					logrus.Warn(err)
				} else {
					allTracks = append(allTracks, tracks...)
				}
			}
		}
	}

	for _, track := range allTracks {
		message += fmt.Sprintf("<b>%v.ls</b> - %v<br>", track.GetID(), track.GetTitle())
	}

	// IDEA: return array of string?
	return message, false, nil
}
